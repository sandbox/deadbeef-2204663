Issue #1450966 deals with a case where Backup and Migrate files attempts to unwrap
a file path into a system path. For files stored on a remote file system such as
cloud storage there is no real file system path. This issue provides a patch that
uses stream wrappers to handle this properly.